const express = require('express');
const hbs = require('hbs');
const port = process.env.PORT || 8080;
var app = express();

app.set('view engine', 'hbs'); 
app.use(express.static(__dirname+'/public'));

app.use((req,res,next)=>{

    var log = `${req.method} ${req.url}`;
    console.log(log);
    next();
})

app.get('/' , (req,res) => {

    res.send('<h1>Home page !</h1>');
   
   
});


app.get('/about' , (req,res) => {

    res.render('about.hbs', {
        pageTitle: 'About Page2'

    });
   
});


app.listen(port, () =>{

    console.log(`app is run on port ${port}`);
});